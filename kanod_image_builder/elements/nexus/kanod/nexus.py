#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from kanod_configure import common


def nexus_hook(arg: common.RunnableParams):
    conf = arg.conf
    nexus = conf.get('nexus', None)
    if nexus is not None:
        nexus_registry = nexus.get('docker', None)
        if nexus.get('insecure', False):
            containers_vars = conf.setdefault('containers', {})
            insecure_registries = (
                containers_vars.setdefault('insecure_registries', []))
            insecure_registries.append(nexus_registry)


common.register('-> Nexus insecure hook', 149, nexus_hook)
