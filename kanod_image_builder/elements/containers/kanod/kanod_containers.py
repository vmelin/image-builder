#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

def get_insecure_registries(conf):
    '''Retrieve the list of insecure registries

    The user provided list may be updated with the nexus itself
    '''
    nexus = conf.get('nexus', None)
    nexus_service = conf.get('nexus_service', None)
    containers_vars = conf.get('containers', {})
    insecure_registries = containers_vars.get('insecure_registries', [])
    if nexus is not None:
        nexus_registry = nexus.get('docker', None)
        if nexus.get('insecure', False) and nexus_registry is not None:
            insecure_registries.append(nexus_registry)
    if nexus_service is not None:
        insecure_registries.append('localhost:8082')
    return insecure_registries
