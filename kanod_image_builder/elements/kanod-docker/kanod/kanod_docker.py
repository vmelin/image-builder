#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


from cloudinit import subp

from kanod_configure import common

from . import kanod_containers


def set_docker_auth(conf):
    '''Set authentication tokens for private registries'''
    containers_vars = conf.get('containers', {})
    raw_auths = containers_vars.get('auths', None)
    k8s_conf = conf.get('kubernetes', None)
    # first destination for docker stand-alone, second for used as kubelet
    # engine
    destination = (
        'root/.docker/config.json' if k8s_conf is None
        else 'var/lib/kubelet/config.json')
    if raw_auths is None:
        return
    auths = [
        {
            "repo": cell.get('repository', ''),
            "token": common.b64(
                cell.get('username', '') + ':' + cell.get('password', '')),
        }
        for cell in raw_auths
    ]
    common.render_template(
        'dockercfg.tmpl',
        destination,
        {'auths': auths}
    )


def container_engine_docker_config(conf):
    '''Configure the docker container engine'''
    proxy_vars = conf.get('proxy')
    if proxy_vars is not None:
        common.render_template(
            'container_engine_proxy.tmpl',
            'etc/systemd/system/docker.service.d/http-proxy.conf',
            proxy_vars
        )
    insecure_registries = kanod_containers.get_insecure_registries(conf)
    mirrors = conf.get('containers', {}).get('registry_mirrors', None)
    common.render_template(
        'docker_daemon.tmpl',
        'etc/docker/daemon.json',
        {'insecure_registries': insecure_registries,
         'registry_mirrors': mirrors}
    )
    set_docker_auth(conf)
    subp.subp(['systemctl', 'daemon-reload'])
    subp.subp(['systemctl', 'enable', 'docker'])
    subp.subp(['systemctl', 'start', 'docker'])


def register_docker_engine(arg: common.RunnableParams):
    engines = arg.system.setdefault('container-engines', {})
    engines['docker'] = container_engine_docker_config


common.register('Register docker engine', 50, register_docker_engine)
