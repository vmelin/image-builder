Core Image
==========
The fields of the core image should be present in most OS images. They
represent the core services that can be configured in a base OS (networking,
ntp)

.. jsonschema:: schema.yaml
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target: